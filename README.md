# CryptoExchangeData

## Description
This package subscribes realtime market data from crypto exchange, then push it to AWS Firehose for storage. It also periodically pulls data from S3, for transformation. 

## Technical Details
By utilizing Python3's Asyncio, we are able to do this in a single event loop. By wrapping data IO functions with async key words, the main thread can easily handle multiple feeds simultaneously. For AWS API call, we use aiobotocore, which also is a wrapper around AWS SDK.


## Tips

install pyenv and pyenv-virtualenv plugin.

```shell
$ brew install pyenv
$ brew install pyenv-virtualenv

# install python 3.6.6
$ pyenv install 3.6.6

# install virtual env for this project
$ pyenv virtualenv 3.6.6 ${workspacedir}

#create .python-version file for workspace
$ pyenv local 3.6.6
```

see https://github.com/pyenv/pyenv-virtualenv for how to activate virtual env.