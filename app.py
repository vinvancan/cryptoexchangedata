#
# app.py
# @author vinvancan@gmail.com
# @description bootstrap crawler
# @created Mon Oct 15 2018 23:09:06 GMT-0700 (PDT)
# @last-modified Sat Nov 03 2018 17:28:02 GMT-0700 (PDT)
#

import logging, logging.config, traceback
logging.config.fileConfig('logging.conf')
LOGGER = logging.getLogger(__name__)

import os, sys, asyncio
from datetime import date
from dotenv import load_dotenv

from concurrent.futures import ThreadPoolExecutor
from cw.cw import CloudWatch
# from tasks.process import run as proc_task


BASEDIR = os.path.abspath(os.path.dirname(__file__))
PATH = os.path.join(BASEDIR, '.env')

if os.path.isfile(PATH) and os.access(PATH, os.R_OK):
    load_dotenv(PATH)

from streaming.crawler import Crawler
from processer.processor import HuobiProcessor


def parse_comma_sep_str(_str):
    pairs = _str.split(',')
    return [_pstr.strip() for _pstr in pairs]


def run():
    STAGE = os.getenv('STAGE')
    SYMBOLS = parse_comma_sep_str(os.getenv('SYMBOLS'))
    PERIODS = parse_comma_sep_str(os.getenv('PERIODS'))
    DEPTHS = parse_comma_sep_str(os.getenv('DEPTHS'))
    STEPS = parse_comma_sep_str(os.getenv('STEPS'))
    STREAM = os.getenv('STREAM')
    DP_STREAM = os.getenv('DP_STREAM')
    REGION = os.getenv('REGION')
    KEY_ID = os.getenv('KEY_ID')
    SECRETE = os.getenv('SECRETE')
    BUFF_SIZE = int(os.getenv('BUFF_SIZE'))

    CloudWatch.REGION_NAME = REGION if REGION else 'us-west-2'
    CloudWatch.AWS_ACCESS_KEY_ID = KEY_ID if KEY_ID else None
    CloudWatch.AWS_SECRET_ACCESS_KEY = SECRETE if SECRETE else None
    CloudWatch.STAGE = STAGE if STAGE else 'DEV'

    while True:
        try:
            LOGGER.debug('starting crawler')
            loop = asyncio.get_event_loop()
            loop.run_until_complete(
                Crawler(
                    symbols=SYMBOLS,
                    periods=PERIODS,
                    dp_symbols=DEPTHS,
                    dp_steps=STEPS,
                    stream_name=STREAM,
                    dp_stream_name=DP_STREAM,
                    region_name=REGION,
                    aws_access_key_id=KEY_ID,
                    aws_secret_access_key=SECRETE,
                    buffer_size=BUFF_SIZE).createCrawlerTask())
            loop.close()

        except Exception as e:
            LOGGER.error(f'restarting task due to exception {e}')
        import time
        time.sleep(5)


def run_processor():
    SRC_BUC = os.getenv('SRC_BUC')
    DEST_BUC = os.getenv('DEST_BUC')
    BASE_DIR = os.getenv('BASE_DIR')
    EXCHANGE = os.getenv('EXCHANGE')

    start_date = date(2018, 10, 16)
    end_date = date(2018, 10, 17)

    LOGGER.info(f'start processor task from {start_date} to {end_date}')

    proc = HuobiProcessor(exchange=EXCHANGE,
        base_dir=BASE_DIR, 
        src_buc=SRC_BUC, 
        dest_buc=DEST_BUC)

    try:
        proc.copy_object(start_date=start_date, end_date=end_date)
    except:
        LOGGER.error(f'error during copy stack trace {traceback.format_exc()}')

    try:
        proc.fetch_and_save(start_date=start_date, 
        end_date=end_date)
    except:
        LOGGER.error(f'error during fetch and save stack trace {traceback.format_exc()}')

    try:
        proc.merge_to_s3()
    except:
        LOGGER.error(f'error merging to s3 {traceback.format_exc()}')
        

    # proc_task(
    #     base_dir=BASE_DIR,
    #     exchange=EXCHANGE,
    #     src_buc=SRC_BUC,
    #     dest_buc=DEST_BUC,
    #     start_date=start_date,
    #     end_date=end_date)


if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == 'processor':
        run_processor()
    else:
        run()
