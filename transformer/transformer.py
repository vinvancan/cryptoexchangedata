
from datetime import datetime
from pytz import utc

TIMEFORMAT = '%Y-%m-%dT%H:%M:%S.%f%z' 
class Transformer(object):
    def transformKline(self, kline):
        pass
    def transformDepth(self, depth, level):
        pass

class HuobiTrans(Transformer):


    def parseKlineDatetime(self, kline):
        kline['startTime'] = datetime.strptime(kline['startTime'], TIMEFORMAT).replace(tzinfo=utc)
        kline['endTime'] = datetime.strptime(kline['endTime'], TIMEFORMAT).replace(tzinfo=utc)
        kline['eventTime'] = datetime.strptime(kline['eventTime'], TIMEFORMAT).replace(tzinfo=utc)
        return kline

    def parseDepthDatetime(self, depth):
        depth['eventTime'] = datetime.strptime(depth['eventTime'], TIMEFORMAT).replace(tzinfo=utc)
        return depth

    def transformKline(self, kline):
        return {
            "symbol": kline['ch'].split('.')[1],
            "startTime": datetime.fromtimestamp(kline['tick']['id'], tz=utc),
            "endTime": datetime.fromtimestamp(kline['tick']['id'] + 59.999, tz=utc),
            "eventTime": datetime.fromtimestamp(kline['ts'] / 1000.0, tz=utc),
            "open": kline['tick']['open'],
            "high": kline['tick']['high'],
            "low": kline['tick']['low'],
            "close": kline['tick']['close'],
            "amount": kline['tick']['amount'],
            "vol": kline['tick']['vol'],
            "count": kline['tick']['count']
        }

    def transformDepth(self, depth, level):
        return {
            "symbol": depth['ch'].split('.')[1],
            "eventTime": datetime.fromtimestamp(depth['ts'] / 1000.0, tz=utc),
            "asks": depth["tick"]["asks"][:level],
            "bids": depth["tick"]["bids"][:level]
        }


