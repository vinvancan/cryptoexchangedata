import os, logging
import csv
from datetime import datetime
import boto3

LOGGER = logging.getLogger(__name__)

class Emitter():

    def emit_klines(self, klines):
        pass

    def emit_depths(self, depths):
        pass

class EmitterFactory():
    @classmethod
    def create(exchange, sink):
        pass

class S3Downloader():
    def __init__(self, base_dir):
        self.s3 = boto3.resource('s3')
        self.base_dir = base_dir
    
    def __ensure_path(self, exchange, data_type):
        try:
            os.mkdir(os.path.join(self.base_dir,exchange))
        except:
            pass
        
        try:
            os.mkdir(os.path.join(self.base_dir,exchange, data_type))
        except:
            pass
    
        return os.path.join(self.base_dir, exchange, data_type)
    
    def download(self, exchange, data_type, 
        bucket_name, key_name, callback):

        file_path = os.path.join(self.__ensure_path(exchange, data_type), key_name.split('/')[-1])

        self.s3.Bucket(bucket_name).download_file(key_name, file_path)

class S3Uploader():
    def __init__(self, base_dir):
        self.s3 = boto3.resource('s3')
        self.base_dir = base_dir

    def upload(self, bucket_name, file_name, exchange, data_name, callback):
        file_path = os.path.join(self.base_dir, exchange, data_name, file_name)
        self.s3.Bucket(bucket_name).upload_file(
            Key=f'{exchange}/{data_name}/{file_name}',
            Filename=file_path,
            Callback=callback)

class WriterManager():
    def __init__(self, exchange, path):
        self.exchange = exchange
        self.path = path
        self.writers = {}

    def emit(self, data, ch):
        if ch not in self.writers:
            self.writers[ch] = CsvFileWriter(self.exchange, ch, self.path)
        
        self.writers[ch].write(data)


class CsvFileWriter():

    kline_names = ["startTime", "endTime", "eventTime", "open",
        "high", "low", "close", "amount", "vol", "count"] 
    depth_names = ['eventTime', 'asks', 'bids']

    def __init__(self, exchange, ch, path):
        self.path = path
        self.exchange = exchange
        ch_split = ch.split('.')
        self.symbol = ch_split[1]
        self.d_type = ch_split[2]
        self.period = ch_split[3]
        self.field_names = self.kline_names if self.d_type == 'kline' else self.depth_names
        self.dir_path = self.__ensure_path()
    
    def __ensure_path(self):
        try:
            os.mkdir(os.path.join(self.path,
                self.exchange))
        except:
            pass
        
        try:
            os.mkdir(os.path.join(self.path,
                self.exchange,self.symbol))
        except:
            pass

        try:
            os.mkdir(os.path.join(self.path,
                self.exchange,self.symbol,
                self.d_type))
        except:
            pass

        try:
            os.mkdir(os.path.join(self.path,
                self.exchange,self.symbol,
                self.d_type,self.period))
        except:
            pass
        
        return os.path.join(self.path,
                self.exchange,self.symbol,
                self.d_type,self.period) 

    def get_file_dir_path(self):
        return os.path.join(self.path, 
            self.exchange, self.symbol, self.d_type, self.period)

    @classmethod
    def get_date_str(cls, dt):
        return datetime.strftime(dt.date(), '%Y-%m-%d')

    @classmethod
    def make_file_name(cls, symbol, dt):
        return f'{symbol}_{cls.get_date_str(dt)}.csv'

    def filter_for_fields(self, data):
        diff = data.keys() - self.field_names
        for d in diff:
            del data[d]
        return data

    def fetch(self, dt):
        path = os.path.join(self.get_file_dir_path(), 
            self.make_file_name(self.symbol, dt))
        return open(path, 'r')

    def write(self, data):
        file_path = os.path.join(self.dir_path, 
            self.make_file_name(data['symbol'], data['eventTime']))

        is_new = False
        if not os.path.exists(file_path):
            is_new = True

        with open(file_path, mode='a') as f:

            writer = csv.DictWriter(f, fieldnames=self.field_names)
            if is_new:
                writer.writeheader()
            writer.writerow(self.filter_for_fields(data))

class HuobiEmitter(Emitter):

    def emit_klines(self, klines):
        pass

    def emit_depths(self, depths):
        pass
