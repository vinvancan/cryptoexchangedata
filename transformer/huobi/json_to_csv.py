"""
Your module description
"""
import json
from datetime import datetime
from collections import OrderedDict as od

import base64

ENCODING='utf8'

COLUMNS = ['startTime', 'eventTime', 'endTime',
    'open','close','low',
    'high','amount','vol',
    'count','final']

def my_handler(event, context):
    output = []
    for record in event['records']:
        dic = json.loads(base64.b64decode(record['data']))
        trans = od()
        
        for cn in COLUMNS:
            if cn in dic['tick']:
                trans[cn] = dic['tick'][cn]
            if cn in dic:
                trans[cn] = dic[cn]
                
        trans['ts'] = datetime.utcfromtimestamp(trans['ts']/1000).isoformat()

        rst_string = ','.join([str(v) for v in trans.values()])
        rst_string += '\n'
        
        b64code = base64.b64encode(rst_string.encode(ENCODING))
        b64str = b64code.decode(ENCODING)
        output.append({
            'recordId': record['recordId'],
            'result': 'Ok',
            'data': b64str,
        })
    return { 'records': output }
