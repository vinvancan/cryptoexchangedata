from collections import deque
from datetime import timedelta, date

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)
      

def walk_folder(Client,Prefix,Bucket,Delimiter='/'):
  rst = Client.list_objects_v2(
      Bucket=Bucket,
      Prefix=Prefix,
      Delimiter=Delimiter)
  lst = []
  try:
    lst = deque(rst['CommonPrefixes'])
  except KeyError:
    pass
     
  while lst:
    pref = lst.popleft()
    pref = pref['Prefix']
    rst = Client.list_objects_v2(
      Bucket=Bucket,
      Prefix=pref,
      Delimiter=Delimiter)
    try:
      lst += rst['CommonPrefixes']
    except KeyError:
      yield rst

