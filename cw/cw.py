import logging
import aiobotocore

LOGGER = logging.getLogger(__name__)


class CloudWatch():
    REGION_NAME = 'us-west-2'
    AWS_ACCESS_KEY_ID = None
    AWS_SECRET_ACCESS_KEY = None
    STAGE = 'DEV'

    @classmethod
    def _count_one_data(cls, names):
        return [{
            'MetricName':
            CloudWatch.STAGE,
            'Dimensions': [{
                'Name': 'COUNTS',
                'Value': str_name
            } for str_name in names],
            'Unit':
            'None',
            'Value':
            1.0
        }]

    @classmethod
    async def countOne(cls, names):
        async with aiobotocore.get_session().create_client(
                service_name='cloudwatch',
                region_name=CloudWatch.REGION_NAME,
                aws_access_key_id=CloudWatch.AWS_ACCESS_KEY_ID,
                aws_secret_access_key=
                    CloudWatch.AWS_SECRET_ACCESS_KEY) as cloudwatch:
            data = CloudWatch._count_one_data(names=names)
            resp = await cloudwatch.put_metric_data(
                MetricData=data,
                Namespace='CRYPTO/CRAWLER')
            LOGGER.debug(resp)
