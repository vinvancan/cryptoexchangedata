import logging, json, base64, gzip, boto3, os, csv, pandas
from io import StringIO
from datetime import timedelta, date
from botocore.exceptions import ClientError
from transformer.huobi.utils import daterange, walk_folder
from transformer.emitter import S3Downloader
from transformer.emitter import WriterManager
from transformer.transformer import HuobiTrans

LOGGER = logging.getLogger(__name__)


class HuobiProcessor():
    def __init__(self, exchange, base_dir, src_buc, dest_buc):
        self.exchange = exchange
        self.base_dir = base_dir
        self.transf = HuobiTrans()
        self.src_buc = src_buc
        self.dest_buc = dest_buc

    def copy_object(self, start_date, end_date):
        client = boto3.client('s3')
        for obj in self._objects_by_dates(
                client=client,
                bucket=self.src_buc,
                start_date=start_date,
                end_date=end_date):
            copy_source = {'Bucket': self.src_buc, 'Key': obj['Key']}
            LOGGER.debug(f'copy object from {copy_source} to {self.dest_buc}')
            client.copy(copy_source, self.dest_buc, obj['Key'])
            LOGGER.debug(f'delete object from {copy_source}')
            client.delete_object(Bucket=self.src_buc, Key=obj['Key'])

    @classmethod
    def _objects_by_dates(cls, bucket, client, start_date, end_date):
        for d in daterange(start_date, end_date):
            prefix = f'huobikline/{d.strftime("%Y/%m/%d")}/'
            rst = client.list_objects_v2(Bucket=bucket, Prefix=prefix)
            LOGGER.info(
                f'fetch {len(rst["Contents"])} objects for prefix {prefix}'
            )
            for obj in rst['Contents']:
                yield obj

    def fetch_and_save(self, start_date, end_date):
        client = boto3.client('s3')
        writer = WriterManager(self.exchange, self.base_dir)
        bucket = self.dest_buc
        for obj in self._objects_by_dates(
                client=client,
                bucket=bucket,
                start_date=start_date,
                end_date=end_date):

            resp = client.get_object(Bucket=bucket, Key=obj['Key'])
            records = str(gzip.decompress(resp['Body'].read()),
                          'utf8').splitlines()

            records = [json.loads(r) for r in records]

            LOGGER.debug(f'save {len(records)} records for object {obj}')

            for r in records:
                if r['ch'].find('kline') >= 0:
                    k_trans = self.transf.transformKline(r)
                    writer.emit(k_trans, r['ch'])
                if r['ch'].find('depth') >= 0:
                    d_trans = self.transf.transformDepth(r, 150)
                    writer.emit(d_trans, r['ch'])

    def get_object_prefix(self, exc, subpath, fil):
        spl = subpath.split('/')
        return f'{exc}/{spl[1]}/{spl[2]}/{spl[3]}/{fil}.gz'

    def merge_to_s3(self):
        client = boto3.client('s3')
        base_dir = os.path.join(self.base_dir, self.exchange)
        for root, _, files in os.walk(base_dir, topdown=False):
            for name in files:
                s3_key = self.get_object_prefix(self.exchange,
                                                root[len(base_dir):], name)
                LOGGER.info(f'merge file {name} to s3 as {s3_key}')
                upload_df = pandas.DataFrame()
                try:
                    resp = client.get_object(Bucket=self.dest_buc, Key=s3_key)

                    upload_df = pandas.read_csv(
                        StringIO(
                            str(gzip.decompress(resp['Body'].read()), 'utf8')))
                    LOGGER.debug(
                        f'download dataframe {upload_df.shape} from s3')
                except ClientError as ex:
                    if ex.response['Error']['Code'] != 'NoSuchKey':
                        raise ex

                with open(os.path.join(root, name), 'r') as csv_f:
                    upload_df = upload_df.append(
                        pandas.read_csv(csv_f)).drop_duplicates(
                            subset={'eventTime'})
                    LOGGER.debug(f'merge as dataframe {upload_df.shape}')

                upload_str = gzip.compress(
                    upload_df.to_csv(index=False).encode('utf8'))
                try:
                    resp = client.put_object(
                        Body=upload_str, Bucket=self.dest_buc, Key=s3_key)
                    LOGGER.debug(f'upload str len f{len(upload_str)}')
                    if resp['ResponseMetadata']['HTTPStatusCode'] != 200:
                        raise Exception(resp['ResponseMetadata'])
                except:
                    raise
