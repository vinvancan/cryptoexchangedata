import os
import json
import unittest
from transformer.transformer import HuobiTrans

class test_transformer(unittest.TestCase):
    test_data_path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), 'inputs')

    def get_test_file_path(self, file_name):
        return os.path.join(self.test_data_path, file_name)

    def test_huobi(self):
        huobi = HuobiTrans()
        with open(self.get_test_file_path('huobiTick.json')) as f:
            input_json = json.load(f)
            kline_trans = [huobi.transformKline(k) for k in input_json['klines']]
            depth_trans = [huobi.transformDepth(d,2) for d in input_json['depths']]
            with open(self.get_test_file_path('huobiTick.trans.json')) as t:
                expected_json = json.load(t)
                exp_kline = [huobi.parseKlineDatetime(k) for k in expected_json['klines']]
                exp_depth = [huobi.parseDepthDatetime(d) for d in expected_json['depths']]

                for exp, act in zip(exp_kline, kline_trans):
                    self.assertDictEqual(exp, act)
                
                for exp, act in zip(exp_depth, depth_trans):
                    self.assertDictEqual(exp,act)


if __name__ == '__main__':
    unittest.main()