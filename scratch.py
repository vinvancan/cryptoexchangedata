#%%
import boto3
from os import walk, scandir

client = boto3.client('s3')
#%%

def process_contents(contents):
  keys = [c['Key'] for c in contents]
  pprint(keys)

rst = client.list_objects_v2(
    Bucket='crypto-market-data-for-fun',
    Prefix='huobikline/2018/10/09/09/',
    Delimiter='/')

process_contents(rst['Contents'])
pprint(rst)


#%%
import boto3
from transformer.huobi.utils import walk_folder
from transformer.emitter import S3Downloader

def callback(counts):
  print(counts)

client = boto3.client('s3')
download = S3Downloader('.')
for f in walk_folder(
  Client=client,
  Prefix='huobikline/',
  Bucket='crypto-market-data-for-fun'):
  for obj in f['Contents']:
    print(obj['Key'])
    


#%%
s3 = boto3.resource('s3')
bucket = s3.Bucket('crypto-market-data-for-fun')

for obj in bucket.objects.filter(Prefix='huobikline'):
  print(obj)

#%%
import json
from transformer.emitter import CsvFileWriter
from transformer.transformer import HuobiTrans
import pandas as pd

huobi = HuobiTrans()
kline_names = ["startTime", "endTime", "eventTime", "open",
    "high", "low", "close", "amount", "vol", "count"]

depth_names = ['eventTime', 'asks', 'bids']

with open('/Users/vinvancan/Projects/cryptoexchangedata/tests/transformer/inputs/huobiTick.json', 'r') as f:
  input_json = json.load(f)
  trans_klines = [huobi.transformKline(k) for k in input_json['klines']]
  writer = CsvFileWriter('huobi','kline','/Users/vinvancan/Projects/cryptoexchangedata/', kline_names)
  writer.write(trans_klines)

  trans_depths = [huobi.transformDepth(d, 100) for d in input_json['depths']]
  writer = CsvFileWriter('huobi','depth','/Users/vinvancan/Projects/cryptoexchangedata/', depth_names)
  writer.write(trans_depths)
  # frame = pd.DataFrame([huobi.transformDepth(k, 150) for k in input_json['depths']])
  # print(frame.to_csv())

#%%
from datetime import timedelta, date
import base64
import gzip
import boto3
from transformer.huobi.utils import daterange, walk_folder
from transformer.emitter import S3Downloader
from transformer.emitter import WriterManager
from transformer.transformer import HuobiTrans

huobi = HuobiTrans()

start_date = date(2018, 10, 16)
end_date = date(2018, 10, 17)

client = boto3.client('s3')
download = S3Downloader('.')
src_buc = 'crypto-market-data-for-fun'

def fetch_and_save(exchange, transf, start_date, end_date):
  writer = WriterManager(exchange,'/Users/vinvancan/Projects/cryptoexchangedata/')
  for single_date in daterange(start_date, end_date):
      common_pref = f'huobikline/{single_date.strftime("%Y/%m/%d")}/'
      rst = client.list_objects_v2(
        Bucket=src_buc,
        Prefix=common_pref)
      
      for obj in rst['Contents']:
        resp = client.get_object(Bucket=src_buc, Key=obj['Key'])
        records = str(gzip.decompress(resp['Body'].read()), 'utf8').splitlines()

        records = [json.loads(r) for r in records]

        for r in records:
          if r['ch'].find('kline') >= 0:
            k_trans = transf.transformKline(r) 
            writer.emit(k_trans,r['ch'])
          if r['ch'].find('depth') >= 0:
            d_trans = transf.transformDepth(r, 150)
            writer.emit(d_trans,r['ch'])

#%%
import os
import gzip
from io import StringIO
import csv
import boto3
from botocore.exceptions import ClientError
client = boto3.client('s3')

exchange = 'huobi'
dest_buc = 'crypto-market-data-csv'
base_dir = '/Users/vinvancan/Projects/cryptoexchangedata'

def get_object_prefix(exc, subpath, fil):
  spl = subpath.split('/')
  return f'{exc}/{spl[1]}/{spl[2]}/{spl[3]}/{fil}.gz'

def merge_to_s3(base_dir, exchange, dest_buc):
  base_dir = os.path.join(base_dir, exchange)
  for root, dirs, files in os.walk(
    base_dir, 
    topdown=False):
    for name in files:
      s3_key = get_object_prefix(exchange, root[len(base_dir):], name)
      print(f'preparing s3_key:{s3_key}')
      upload = ''
      try:
        resp = client.get_object(
          Bucket=dest_buc,
          Key=s3_key)
          
        upload = str(gzip.decompress(resp['Body'].read()), 'utf8')
        print(f'download {len(upload)} chars from s3')
      except ClientError as ex:
        if ex.response['Error']['Code'] != 'NoSuchKey':
          raise ex
      
      with open(os.path.join(root, name), 'r') as csv_f:
        from_file = csv_f.read()
        print(f'read {len(from_file)} from file')
        if upload:
          upload += from_file[from_file.index('\n'):]
        else:
          upload = from_file
        print(f'merge as len {len(upload)}')
      
      upload = gzip.compress(upload.encode('utf8'))
      try:
        resp = client.put_object(Body=upload, Bucket=dest_buc, Key=s3_key)
        return resp['ResponseMetadata']['HTTPStatusCode'] == 200
      except:
        raise









#%%
from processer.processor import HuobiProcessor

proc = HuobiProcessor(
  '.', 
  'crypto-market-data-for-fun', 
  'crypto-market-data-csv')


proc.merge_to_s3()


#%%
from io import StringIO
import csv
import pandas as pd
empty = pandas.DataFrame()

with open('./huobi/btcusdt/kline/1min/btcusdt_2018-10-17.csv') as f:
  tmp1 = pd.read_csv(f)
  tmp1.append(empty)
  print(tmp1.shape)
  print(empty.shape)
  # csv_buff = tmp1.to_csv(index=False)
  # tmp2 = pd.read_csv(StringIO(csv_buff))
  # print(tmp2.to_csv())
  # print(tmp2.duplicated())
  
# dat1 = pd.DataFrame.from_csv(
#   './huobi/btcusdt/kline/1min/btcusdt_2018-10-17.csv')
# dat2 = pd.DataFrame.from_csv(
#   './huobi/btcusdt/kline/1min/btcusdt_2018-10-17.csv')

# dat3 = dat1.append(dat2)
# print(dat1.shape)
# print(dat1.duplicated())
# print(dat2.shape)
# print(dat3.shape)
# dat3 = dat1.drop_duplicates(subset={'eventTime'})
# print(dat3.shape)

# print(dat1.to_csv())

#%%
import pandas


print(tmp.empty)