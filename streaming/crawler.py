#
# crawler.py
# @author VL
# @description live market data subscribing from huobi exchange
# @created Mon Oct 08 2018 17:55:44 GMT-0700 (PDT)
# @last-modified Wed Oct 24 2018 21:15:50 GMT-0700 (PDT)
#

import asyncio, json, threading, logging, traceback, os
from asyncio import Queue, TimeoutError
from concurrent.futures import FIRST_EXCEPTION
from streaming.lib.huobi import subscribe
from cw.cw import CloudWatch
from datetime import datetime
import time, threading
from random import randint
import pytz
import aiobotocore

LOGGER = logging.getLogger(__name__)


class Crawler(object):
    def __init__(self,
                 symbols,
                 periods,
                 dp_symbols,
                 dp_steps,
                 stream_name,
                 dp_stream_name,
                 region_name,
                 aws_access_key_id,
                 aws_secret_access_key,
                 buffer_size,
                 batch_interval=5):
        '''
        :symbols array of symbols
        :periods array of periods
        :buffer_size must be smaller than 500
        :stream_name firehose stream name
        :region_name aws region name
        :aws_access_key_id 
        :aws_secret_access_key
        :buffer_size batch size to put record to fire hose
        :batch_interval timeout seconds waiting for a single record
        '''
        self.sub_template = "market.%s.kline.%s"
        self.depth_template = "market.%s.depth.%s"
        self.symbols = symbols
        self.periods = periods
        self.dp_symbols = dp_symbols
        self.dp_steps = dp_steps
        self.stream_name = stream_name
        self.dp_stream_name = dp_stream_name
        self.region_name = region_name
        self.aws_access_key_id = aws_access_key_id
        self.aws_secret_access_key = aws_secret_access_key
        self.buffer_size = buffer_size
        self.batch_interval = batch_interval
        LOGGER.info(f'contr crawler {self.__dict__}')

    async def send_to_firehose(self, records, fh_name):
        '''
        send with retry, retry untill all records are sent
        :records array of records to send
        '''
        aiobotocore.get_session()

        async with aiobotocore.get_session().create_client(
                service_name='firehose',
                region_name=self.region_name,
                aws_access_key_id=self.aws_access_key_id,
                aws_secret_access_key=self.aws_secret_access_key) as client:
            response = await client.put_record_batch(
                DeliveryStreamName=fh_name, Records=records)

        LOGGER.info(f'send {len(records)} record to firehose {fh_name}')
        if response['FailedPutCount']:
            # if any records failed, retry record untill all succeeds
            LOGGER.warning(f'failed count {response["FailedPutCount"]}')
            retry_data = []
            for idx, rec_resp in enumerate(response['RequestResponses']):
                if 'RecordId' not in rec_resp or not rec_resp['RecordId']:
                    retry_data.append(records[idx])
            await self.send_to_firehose(retry_data, fh_name)

    async def process_data(self, queue, fh_name):
        '''
        process ticks which was subscribed, this function never returns.
        This function wait for a certain timeout for incoming record, if timeout, 
        send already recieved record to firehose. If records number exceeds buffer_size
        send them all to firehose.
        '''
        while True:
            records = []
            try:
                while len(records) < self.buffer_size:
                    rec = await asyncio.wait_for(
                        queue.get(), timeout=self.batch_interval)
                    records.append(rec)
            except TimeoutError:
                # queue are empty currently no data in it, should proceed and
                # send to firehose
                pass
            LOGGER.debug(
                f'getting {len(records)} records from buffer, {queue.qsize()} remaining'
            )
            if records:
                await self.send_to_firehose(records, fh_name)

    async def kline_callback(self, data):
        '''
        marshall incoming data, append line seperator, then put into the blocking queue
        '''
        data = {'Data': json.dumps(data) + '\n'}
        await self.klines.put(data)

    async def depth_callback(self, data):
        data = {'Data': json.dumps(data) + '\n'}
        await self.dps.put(data)

    def on_close(self, msg):
        LOGGER.warning(msg)

    def on_error(self, msg):
        LOGGER.error(msg)
        raise Exception('unexpected error')

    async def createCrawlerTask(self):
        '''
        subscribe to live market data by websocked, start process kline task.
        '''
        sub = {}
        for s in self.symbols:
            for p in self.periods:
                subStr = self.sub_template % (s, p)
                sub[subStr] = {'callback': self.kline_callback}

        for d in self.dp_symbols:
            for s in self.dp_steps:
                subStr = self.depth_template % (d, s)
                sub[subStr] = {'callback': self.depth_callback}
        LOGGER.info(f'subscribed to huobi')
        LOGGER.info(f'symbols {self.symbols} periods {self.periods}')
        LOGGER.info(f'depths {self.dp_symbols} periods {self.dp_steps}')
        self.klines = Queue()
        self.dps = Queue()
        done, not_done = await asyncio.wait([
            CloudWatch.countOne(names=['START']),
            self.process_data(self.klines, self.stream_name),
            self.process_data(self.dps, self.dp_stream_name),
            subscribe(sub, on_close=self.on_close, on_error=self.on_error)
        ],
                                            return_when=FIRST_EXCEPTION)

        for _nd in not_done:
            _nd.cancel()

        # raise exception here then restart task
        raise Exception(f'task {list(done)} ended')
